export function utils() {

    const base64ToBlob = async (dataUrl, fileName) => {
        const res = await fetch(dataUrl);
        const blob = await res.blob();
        return new File([blob], fileName, { type: 'image/jpeg' });
    }

    const blobToBase64 = (blob) => {
        return new Promise((resolve, _) => {
            const reader = new FileReader()
            reader.onloadend = () => resolve(reader.result);
            reader.readAsDataURL(blob)
        })
    }


    const download = (content, fileName, contentType) => {
        var a = document.createElement("a");
        var file = new Blob([content], {type: contentType});
        a.href = URL.createObjectURL(file);
        a.download = fileName;
        a.click();
    }

    return {base64ToBlob, blobToBase64, download}
}
