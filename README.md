# ANNO - To annotate videos

Tools influence practices influence tools! ANNO is a lightweight browser application to annotate media files. It work with video and audio, and it's perfect to send feedbacks to your friends or coworkers.

From the same collection see [story-baobab](https://hub.xpub.nl/soupboat/sb/), a little tool to sketch storyboards.

## Getting started

To start using ANNO drop a media file into the main view. Notice that the workspace is divided in two: on the left is the video area, on the right there is space for notes. 

Once you _drag&dropped_ the video you are ready to start. Just watch the contents and take notes in the right panel. 

Don't worry about the order of the notes: ANNO takes care of sorting them following the timecode of the video. Your notetaking can be as messy as possible, the application will manage orders and timecodes, in order to produce something legible also for others. 

If you wanna return to a previous note, just click on it! The video player will scrub till there, letting you rewatch and adjust the note accordingly.

## Development

ANNO is a browser-based application developed with Vite and Vue 3. To develop just clone the repo, install with `yarn` and run the development server with `yarn dev`. 

Contributions and forks are very welcomed!

## Roadmap

- Export notes
    - .pdf (web to print)
    - .SRT or similar (to import directly in video editors)
    - .json (to reimport in ANNO and edit / navigate)
- Edit note
- Tagging system ( a system to tag scenes like @lorem @ipsum and then having a list of all the categories / scenes / tags) 

